# Chat para 24H24L

Pasos:

1. Requisito: tener instalado docker
2. Lanzar ejabberd en un contenedor docker
3. Configurar un servidor web que proxee las peticiones al contenedor
4. Modificar el html/javascript en 24h24l.org

Se emplea como servidor jabber [ejabberd](https://ejabberd.im), y para embeber en la web el servicio la librería de javascript [converse.js](https://conversejs.org/). El servicio de chat que se pretende embeber es similar a la demo que tiene converse.js en: https://conversejs.org/demo/embedded.html

Se configuró el servidor ejabberd para permitir conexiones anónimas y escucha en el puerto 5280 (protocolo BOSH o http-bind) en localhost. Para acceder al puerto se proxea con un servidor web con apache o nginx. El servidor web escucha en el puerto 80 y proxea al puerto 5280, de esta forma no exponemos los demás puertos de ejabberd y evitamos problemas de dominios cruzados (CORS) con la librería converse.js

```
          +----------------------------+
          |                            |
internet  +-------+   +--------------+ |
 +------->-  nginx|   |ejabberd      | |
          |  :80  +-->+localhost:5280| |
          |       |   |              | |
          +-------+   +--------------+ |
          |                            |
          +----------------------------+
```

## Contenedor Docker de ejabberd

Para simplificar la instalación de ejabberd hago uso de una imagen docker de ejabberd y monto un volumen con la configuración que está en el directorio `conf/ejabberd.yml`.

```bash
# Descargar imagen
docker pull ejabberd/ecs

# Arrancar contenedor con ejabberd sobre directorio que contiene conf/ y logs/
docker run -d --name ejabberd \
  -v `pwd`/conf/ejabberd.yml:/home/ejabberd/conf/ejabberd.yml \
  -v `pwd`/logs:/home/ejabberd/logs \
  -p 5280:5280 ejabberd/ecs
```

> Es posible que también se pueda ejecutar con `podman` en lugar de `docker`.

## Reverse proxy

El contenedor docker expone el puerto 5280 que escucha en localhost y que **NO** nos interesa exponer a internet. Para que funcione el chat proxeamos con nginx o apache un path que apunte a `http://localhost:5280/bosh/`.

```conf
server {
  #...

  # Uso un path ininteligible cualquiera para evitar que encuentren el servicio con scanners web
  # este path será necesario configurarlo en el script  
  location /ekohria4bequahLu/ { # location /http-bind/
    proxy_pass http://localhost:5280/bosh/;
  }
}
```

## Script de converse.js

> Ver el fichero `chat.html` para un ejemplo completo.

```js
converse.initialize({
    authentication: 'anonymous',
    auto_login: true,
    auto_join_rooms: [
        '24h24l@conference.localhost', // Nombre de la sala a la que se une automáticamente
    ],
    bosh_service_url: 'http://localhost/bosh/', // ««« Modificar con la URL donde está proxeado el servicio BOSH
    jid: 'localhost', // Dominio del servidor ejabberd, dejar!!! como localhost
    notify_all_room_messages: [
        '24h24l@conference.localhost',
    ],
    prebind: false,
    singleton: true,
    show_client_info: false,
    show_controlbox_by_default: false,
    locales_url: "/ruta-a-converse.js/locales/es-LC_MESSAGES-converse-po.js", // «««« Modificar con la ruta adecuada
    view_mode: 'embedded'
});
```

## Comandos de ejabberctl (NO es necesario para la configuración)

Comandos para modificar opciones con ejabberd.

> WARNING!!! NO hace falta ejecutar ninguno para que funcione la configuración anterior, las salas se crean automáticamente, el acceso es anónimo con lo cual no requiere ningún usuario, no se requiere ninguna configuración adicional de la sala, ...

```bash
# Entrar en el contenedor (alpine linux, NO! bash, usar ash)
docker exec -it ejabberd ash

# Estado del servidor jabber
./bin/ejabberdctl status

# Cambiar la contraseña del usuario admin
./bin/ejabberdctl register admin localhost passw0rd

# Crear cuenta pública
./bin/ejabberdctl register user24h24l localhost Eigh3ieXo7oh

# Crear una sala
./bin/ejabberdctl create_room chat24h24l localhost localhost

# Establecer la membresía del usuario a la sala
./bin/ejabberdctl set_room_affiliation chat24h24l localhost user24h24l@localhost participant

# Recuperar las opciones de la sala
./bin/ejabberdctl get_room_options chat24h24l localhost

# Modificar propiedades de la sala
./bin/ejabberdctl change_room_option chat24h24l localhost persistent true
./bin/ejabberdctl change_room_option chat24h24l localhost mam true
./bin/ejabberdctl change_room_option chat24h24l localhost title "Chat 24H24L"
./bin/ejabberdctl change_room_option chat24h24l localhost allow_user_invites true

# Eliminar sala
./bin/ejabberdctl destroy_room 24h24l conference.localhost

# Listar salas 24h24l@conference.localhost
./bin/ejabberdctl muc_online_rooms global

# Establecer título da sala
./bin/ejabberdctl change_room_option 24h24l conference.localhost title "Chat 24H24L"

./bin/ejabberdctl connected_users

./bin/ejabberdctl get_room_affiliation 24h24l conference.localhost 630977791394096538512898@localhost/812586885823787009412930

./bin/ejabberdctl set_room_affiliation 24h24l conference.localhost 630977791394096538512898@localhost/812586885823787009412930 [none|admin|owner|member]
./bin/ejabberdctl set_room_affiliation 24h24l conference.localhost 630977791394096538512898@localhost/812586885823787009412930 member
```

```
# Buscar el JID completo, si no se pone nada muestra todos los usuarios
./search_user.sh 1015

# Cambiar affiliation (admin|member|none|owner) de un usuario recuperado de la lista anterior
./set_affiliation.sh 101526872793948461109699@localhost/1351957327667954333842818 admin

# Si un usuario molesta puede silenciarse cambiándole affiliation a none
./set_affiliation.sh 101526872793948461109699@localhost/1351957327667954333842818 none
```

.modal-backdrop.show