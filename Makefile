SOURCE=./adoc/*.adoc

release: clean html

html:
	mkdir -p ./public/images ./public/audios ./public/assets ./public/dist
	cp -Rf ./adoc/images/* ./public/images/ 
	cp -Rf ./adoc/assets/* ./public/assets/
	cp -Rf ./adoc/audios/* ./public/audios/
	cp -Rf ./adoc/dist/* ./public/dist/ 
	asciidoctor -a linkcss --doctype article --source-dir ./adoc --destination-dir ./public $(SOURCE)

clean:
	rm -Rf ./public/*
