const converseDivElement = document.querySelector("#conversejs");
if (converseDivElement) {
  var cfgConverse = {
      allow_logout: true,
      authentication: 'anonymous',
      assets_path: '/dist/',
      auto_login: true,
      auto_join_rooms: [
          '24h24l@conference.localhost',
      ],
      // bosh_service_url: 'https://desenvolvemento.software/ekohria4bequahLu/',
      bosh_service_url: 'https://chat.desenvolvemento.software/ekohria4bequahLu/',
      debug: true,
      jid: 'localhost', 
      notify_all_room_messages: [
          '24h24l@conference.localhost',
      ],
      prebind: false,
      singleton: true,
      show_client_info: false,
      show_controlbox_by_default: false,
      locales_url: "/dist/locales/es-LC_MESSAGES-converse-po.js",
      view_mode: 'embedded',
      theme: 'concord'
  };

  // function createButtonClose() {
  //   var tempDiv = document.createElement('button');
  //   tempDiv.className += 'btn btn-success btn-lg btn-block';
  //   tempDiv.innerHTML = 'Cerrar chat';
  //   tempDiv.addEventListener("click", function(evt) {

  //   }, false);
  //   converseDivElement.insertAdjacentElement('afterend',tempDiv);
  // }

  if (!window.matchMedia("only screen and (max-device-width: 767px)").matches) {
    converse.initialize(cfgConverse); 
  } else {
    converseDivElement.style.position = 'relative';

    const startChatButton = document.querySelector("#conversejs>button");
    startChatButton.addEventListener("click", function(evt) {
      converseDivElement.style.display = 'fixed';
      cfgConverse.view_mode = 'mobile';
      cfgConverse.sticky_controlbox = false;
      converse.initialize(cfgConverse);
    }, false);
    startChatButton.style.display = 'block';
  }
}