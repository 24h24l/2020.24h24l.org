var playerConfig = {  // Localisation
  i18n: {
    restart: 'Reiniciar',
    rewind: 'Rebobinar {seektime}s',
    play: 'Play',
    pause: 'Pausa',
    fastForward: 'Avanzar {seektime}s',
    seek: 'Saltar',
    seekLabel: '{currentTime} de {duration}',
    played: 'Played',
    buffered: 'Almacenado',
    currentTime: 'Current time',
    duration: 'Duración',
    volume: 'Volumen',
    mute: 'Silencio',
    unmute: 'Unmute',
    download: 'Descargar',
    frameTitle: 'Reproductor para {title}',
    settings: 'Ajustes',
    pip: 'PIP',
    menuBack: 'Ir al menú previo',
    speed: 'Velocidad',
    normal: 'Normal',
    quality: 'Calidad',
    loop: 'Bucle',
    start: 'Inicio',
    end: 'Fin',
    all: 'Todo',
    reset: 'Reset',
    disabled: 'Deshabilitado',
    enabled: 'Habilitado',
    advertisement: 'Ad',
    qualityBadge: {
      2160: '4K',
      1440: 'HD',
      1080: 'HD',
      720: 'HD',
      576: 'SD',
      480: 'SD',
    }
  },
  autoplay: true,
  settings: ['quality', 'speed'],
  controls : [
    'play',
    'progress',
    'current-time',
    'volume'
  ]
};

var player = null;

function createPlayer(cfg) {
  if (player != null) {
    player.destroy();
  }
  player = new Plyr('.player audio', cfg);
  document.querySelector('.player img').addEventListener("click", function() {
    player.togglePlay();
  });
}

if (!window.matchMedia("only screen and (max-device-width: 480px)").matches) {
  createPlayer(playerConfig);
} else {
  var cfg = playerConfig;
  cfg.controls = ['play','progress','current-time','volume'];
  createPlayer(cfg);
}

function getMetadata() {
  jQuery.ajax({
    url: "https://vifito.eu/player.php",
    jsonp: "callback", 
    dataType: "jsonp",
    success: function( response ) {
      if (response["title"] === undefined) {
        return;
      } 
      //jQuery('.player .widget h2').html('[' + response.horario + ']  ' + response.title);
      jQuery('.player .widget h2').html('[' + response.category + '] ' + response.title);
      if (jQuery('.player .widget h2+p').length == 0) {
        jQuery('.player .widget h2').after('<p></p>');
      }
      jQuery('.player .widget h2+p').html(
        '<strong>Participante(s):</strong> ' + response.participantes + ' <br/>' + 
        '<strong>Moderador:</strong> ' + response.moderador 
      );
    }
  });
}

setInterval(getMetadata, 60000);
getMetadata();