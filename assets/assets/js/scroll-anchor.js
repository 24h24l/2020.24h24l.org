$(document).ready(function() {
  function setScrollTop() {
    var hash = window.location.hash.slice(1); 
    if (hash != '') {
      if (($(window).scrollTop() - 80) < $(":target").offset().top) {
        var y = $(window).scrollTop(); 
        $(window).scrollTop(y - 90);
        $(':target').parent().css({"backgroundColor": "#eeeeee"});
      }
    }
  }
  $(window).bind('hashchange', function () { 
    setScrollTop();
  });
  setScrollTop();
});