= Ponentes y Moderadores
include::includes/header.adoc[]
:jbake-type: page
:jbake-tags: 
:jbake-status: published
:withtitle: true

image::images/banner-moderador.jpg[link=index.html]

== link:empresa.html[Empresa]

*Moderadores:* link:ponente-01.html#_jorge_lama[Jorge Lama], link:ponente-01.html#_david_marzal[David Marzal] y link:ponente-01.html#_jose_jiménez[Jose Jiménez]

1. link:empresa.html[Licencias libres y empresas] : Bárbara Román y María Cerviño de https://www.nolegaltech.com/[Nolegaltech,window="_blank"], Laura Castro
2. link:empresa.html[Servicios software libre y empresas] : Carlos Rodríguez (https://twitter.com/capri99[@capri99,window="_blank"]) CEO de https://www.librebit.com/[Librebit,window="_blank"], Jose Miguel Parrella(https://twitter.com/bureado[@bureado, windows="_blank"]) Microsoft
y Xavier Castaño(https://twitter.com/xcastanho[@xcastanho]) de https://www.igalia.com/[Igalia].
3. link:empresa.html[Ordenadores con Linux] : Alejandro (https://twitter.com/alejandroxyz123[@alejandroxyz123,window="_blank"]) empresa https://slimbook.es/[Slimbook]
4. link:empresa.html[Migraciones] : David Marzal (https://mastodon.social/@DavidMarzalC[@DavidMarzalC,window="blank"]) e Ivan Terceros (https://twitter.com/ivntres[@ivntres, windows="_blank"])
5. link:empresa.html[Administraciones públicas y GNU/Linux] : https://www.kdeblog.com/kde-blog[Baltasar,window="_blank"], https://twitter.com/saznar[Salvador Aznar,window="_blank"], https://mastodon.social/web/accounts/141842[Tàfol Nebot,window="_blank"], https://twitter.com/jmoyanoreiz[Julian Moyano,window="_blank"]


== link:hardware.html[Hardware]

*Moderadores:*  link:ponente-01.html_alvaro_martinez[Alvaro Martinez] y link:ponente-01.html#_rúben_gómez[Rubén Gómez]

1. link:hardware.html[Linux y hardware antiguo] : Carlos Encalada(https://twitter.com/karlosrocker[@KarlosRocker,window="_blank"]) y SantiRey_TICs(https://twitter.com/santirey_tics[@SantiRey_TICs,window=""_blank])
2. link:hardware.html[Placas ARM(SBC)] : Atareao(https://www.atareao.es/[https://www.atareao.es/,window="_blank"]) y Angel (https://ugeek.github.io/[https://ugeek.github.io/,window="_blank"])
3. link:hardware.html[Linux y router, openwrt, ddwrt] : Carlos (https://elblogdelazaro.gitlab.io/[https://elblogdelazaro.gitlab.io/,window="_blank"])
4. link:hardware.html[Domótica] : Diego Muñoz(https://www.diegomunozbeltran.com/[https://www.diegomunozbeltran.com/es/,window="_blank"]) y German Martin(https://t.me/gmag11[@gmag11,window="_blank"])


== link:linux.html[GNU/Linux]

*Moderadores:*  link:ponente-01.html#_rubén_gómez[Rubén Gómez] y link:ponente-01.html#_juan_febles[Juan Febles]

1. link:linux.html[Filosofía del software] : Fernando la chica(https://t.me/flachica[@flachica,window="_blank"]) y David Moltalva
2. link:linux.html[Escritorio en GNU/Linux] : Diego Muñoz con i3, Aleix Pol(https://twitter.com/AleixPol[@AleixPol]) de KDE, Juanjo Salvador(https://twitter.com/Linuxneitor[@Linuxneitor]) Gnome
y Alejandro(Telegram https://t.me/alex_dmm[@alex_dmm]) con i3.
3. link:linux.html[Distribuciones de Linux] : https://www.diegomunozbeltran.com/en/[Diego Roman,window="_blank"], Oskar Torres
4. link:linux.html[Software libre y propietario en Linux] : Integrantes del podcast https://www.youtube.com/playlist?list=PLrtOacI496LQE-2js3gwgv2upN72zJpO8[Ubuntu y otras hierbas,window="_blank"]

== link:multimedia.html[Multimedia]

*Moderadores:* link:ponente-01.html#_jose_picón[José Picón] y link:ponente-01.html#_jorge_lama[Jorge Lama]

1. link:multimedia.html[Grabar un podcast en Linux] : Javier Teruelo y Ritxi.
2. link:multimedia.html[Música en Linux] : José GDF (https://twitter.com/JoseGDF[@JoseGDF]), Miguel Muiños (https://twitter.com/mianromu[@mianromu]) y Carlos Arturo Guerra(https://twitter.com/GuerraCarlosA[@GuerraCarlosA, window="_blank"])
3. link:multimedia.html[Edición de video] : Juan Febles https://twitter.com/juan__febles[@juan_febles,window="_blank"] y Álvaro Martínez (https://twitter.com/DioxCorp[@Dioxcorp,window="_blank"])
4. link:multimedia.html[Diseño gráfico] : Álvaro Martínez (https://twitter.com/DioxCorp[@Dioxcorp,window="_blank"])

== link:programacion.html[Programación]

*Moderadores:* link:ponente-01.html#_david_vaquero[David Vaquero] y link:ponente-01.html#_jose_jiménez[Jose Jiménez].

1. link:programacion.html[Programar en Linux] : Joseda, Diego Roman, Kapis, David Roman
2. link:programacion.html[Despliegue de aplicaciones] : Ignasi y Edu del podcast https://www.entredevyops.es/[Entre Dev y Ops]
3. link:programacion.html[Programación en Scripts,bash y zsh] : Mauro Chojrin (https://twitter.com/mchojrin[@mchojrin])
4. link:programacion.html[VPS] : Raul (https://twitter.com/rapejim[@Rapejim])

== link:redes.html[Redes y Seguridad]

*Moderadores:*  link:ponente-01.html#_jose_jiménez[Jose Jiménez], link:ponente-01.html#_samuel[Samuel] y link:ponente-01.html#_eduardo_collado[Eduardo Collado]

1. link:redes.html[Hosting y Linux] : Jorge(@cegroj) y @Al_Demon
2. link:redes.html[Monitorización redes] : Eduardo Taboada y Samuel (https://twitter.com/yovirtualizador[@Yovirtualizador])
3. link:redes.html[Seguridad(VPN, auditorias..)] : Oscar Rodríguez https://twitter.com/josuer08[@josuer08,window="_blank"]
