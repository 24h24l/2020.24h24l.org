= Emisión en directo
include::includes/header.adoc[]
:jbake-type: page
:jbake-tags: 
:jbake-status: published

image::images/banner.png[link=index.html]

[.jumbotron]
====
+++<i class="fa fa-podcast" aria-hidden="true"></i>+++ La emisión en directo ya finalizó. Próximamente se publicarán todos los audios del evento en  link:https://anchor.fm/[anchor.fm], link:https://www.youtube.com/channel/UCxUKfsev_8aJEQHFKVMk0Kw[youtube] y link:https://t.me/evento24h24l[telegram]. 

Gracias por participar
====

== Feedback

Déjanos tus comentarios sobre el evento **24H24L**

++++
<script async src="https://comments.app/js/widget.js?3" data-comments-app-website="K-5irx_U" data-limit="5" data-color="CA9C0E" data-outlined="1"></script>
++++
