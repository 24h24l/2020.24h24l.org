= Moderadores
include::includes/header.adoc[]
:jbake-type: page
:jbake-tags: 
:jbake-status: published
//:notitle:

image::images/banner-moderador.jpg[link=hardware.html]
//include::includes/menu.adoc[]

== Jose Jiménez
Formador, entusiasta Telegram y creador de varios podcast , **Tomando un Café** link:https://anchor.fm/tomandouncafe[https://anchor.fm/tomandouncafe,window="_blank"], **ARM para TODOS**  link:https://anchor.fm/arm-para-todos[https://anchor.fm/arm-para-todos,window="_blank"]
,  **Aviones de Papel** link:https://anchor.fm/avionesdepapel[https://anchor.fm/avionesdepapel,window="_blank"] y **Formadores en Tiempos Revueltos**, junto a David Vaquero. Tengo múltiples canales de Telegram : https://t.me/entrevistaendiferido[Entrevista En Diferido], https://t.me/UnDiaUnaAplicacion[Un Dia Una Aplicación], https://t.me/aprendepython[Aprende Python] y https://t.me/UnPythonAlDia[Un Python al Día], también un par de grupos.


== Eduardo Collado

Máster en Sociedad de la Información e Ingeniero Técnico Informático.
Llegué a certificarme en Cisco Cisco CCNP, CCDA, CCNA, CSE, SMB Specialization for Engineers, CXFF, CXFS, CXFA,y CCAI (Instructor), también logué aprobar y recertificar tres veces el CCIE R&S 350-001.

Actualmente Socio cofundador de Tecnocrática Centro de Datos, S.L. empresa de Datacenter y hosting bajo la marca Neodigit estando entre los primeros registradores de dominios de España.

Podcaster de **https://www.ivoox.com/podcast-podcast-eduardo-collado_sq_f1448486_1.html[Podcast de Eduardo Collado,,window="_blank"]**.

link:https://www.eduardocollado.com/[Web,,window="_blank"] y Twitter como https://twitter.com/ecollado[@ecollado,,window="_blank"]

== Jorge Lama
Friki confesado, apasionado de las nuevas tecnologías. Diseñador sonoro y productor independiente de podcasting con ganas de volverse Content curator.
Creador de contenidos multimedia y consultor de software libre en https://icarto.es/[iCarto,window="_blank"].
Defensor de la Cultura libre, https://twitter.com/raivenra[@raivenra, window="_blank"] en redes sociales.

== Alvaro Martinez
Buenas, me llamo Alvaro Martinez. Soy un Freak amante de la tecnología y del sistema operativo del pingüino, siempre he defendido los estándares lo máximo posible.
Mi trabajo es detrás de un pc y de las cámaras, cuando tengo un rato y algo interesante que contar lo digo en mi canal de https://www.youtube.com/channel/UC5xnm-eHlFRO8EEV9gTaNJw[YouTube,window="_blank"]. También mantengo un https://www.ivoox.com/podcast-foreveralonepodcast_sq_f119948_1.html[podcast,window="_blank"] que no deja indiferente a nadie y Twitter https://twitter.com/DioxCorp[@DioxCorp,window="_blank"]

== David Marzal
Administrador de sistemas GNU/Linux de profesión, apasionado por el software libre y la sostenibiliad de https://mastodon.social/@DavidMarzalC[vocación]. Intentando poner mi granito de arena para tener un mundo y una sociedad mejor y más libre.

* https://gitlab.com/listados/awesome-telegram -> Listado de Canales, Grupos, Bots y Recursos de Telegram de todo tipo pero con un especial foco en el FLOSS y la sostenibilidad
** https://t.me/ResiduoCero[@ResiduoCero,window="_blank"] a nivel nacional y https://t.me/ResiduoCeroRM[@ResiduoCeroRM,window="_blank"] a nivel regional
* link:https://residuocerorm.gitlab.io[https://residuocerorm.gitlab.io,window="_blank"] -> Asociación Residuo Cero Región de Murcia
* link:https://gnulinuxvalencia.org[https://gnulinuxvalencia.org,window="_blank"] -> Colaborador adoptivo del Podcast de la Asociación GNU/Linux Valencia

== Juan Febles
Mi nombre es Juan Febles y me encanta enseñar aprendiendo y aprender enseñando.
Soy maestro de Educación Especial (Profesor de Apoyo) en un colegio de Tenerife
Me encanta el Software Libre
Realizo desde 2016 un Podcast sobre GNU/Linux: Podcast Linux.
Podcast Linux es un proyecto para acercar el mundo GNU/Linux y el Software Libre a todas las personas que quieran conocerlo y disfrutarlo. Un podcast para amantes del Ñu y el Pingüino.

Puedes contactar de las siguientes maneras:

- Twitter: https://twitter.com/podcastlinux[@podcastlinux,window="_blank"]
- Mastodon: https://mastodon.social/@podcastlinux[@podcastlinux,window="_blank"]
- Correo: podcastlinux@disroot.org
- Archive.org: https://archive.org/details/@podcast_linux[@podcast_linux,window="_blank"]
- Gitlab: https://gitlab.com/podcastlinux[@podcastlinux,window="_blank"]
- Web: link:podcastlinux.com[podcastlinux.com,window="_blank"]


Cacharreo y pruebo todo lo que puedo y me gusta conocer y compartir lo que aprendo con los demás.


== Samuel

Samquejo soy yo. El autor de todo este batiburrillo de blog, podcasts y videos.

Soy consultor de sistemas, tanto Windows como Linux (con preferencia por las derivadas de RedHat) y consultor de virtualización.

YoVirtualizador, AKA Sistemas, AKA aITi (lease Haití, gracias compañeros) es mi personaje en todas las historias.

Ni confirmo ni desmiento que las historias en las que YoVirtualizador está involucrado son completamente ciertas o falsas, sean reales o adaptadas de las diversas realidades que nos rodean, que posean cierta retórica y que el espíritu del BOFH siempre esté presente.

YoVirtualizador es mi personaje para el blog, tanto el antíguo como este nuevo, que es experto en diversas tecnologías, y BOFH por necesidad e inspiración.

Nace de un proyecto fallido a principios de siglo que se interrumpió porque patata y renació en el blog de blogger que tengo en los enlaces.

Por todo esto, y algo más, larga vida a https://t.me/yovirtualizador[@YoVirtualizador,window="_blank"]

== Jose Picón
Soy jmpicon miembro de GNU/Linux Valencia y moderador de los programas que hacemos en la parte Social donde tratamos de todos los temas Sociales que nos preocupan.
Apasionado de la tecnología y en especial del Software libre.
En mis inicios empece con un Spectrum 128k y aprendí a programar en Basic.
Empece con Linux a finales de los 90, y siempre he ido cambiando de distribuciones, al mismo tiempo trabajando con apple.
Soy amante de los podcast desde el principio y siempre he querido tener mi propio podcast donde hablar de las cosas que me apasionan. Este año gracias a GNU/Linux Valencia empece en este mundo del podcast.
Ahora tengo mi propio canal que es jmpicon y estoy en los inicios para ir aprendiendo cada día mas.

Mi Web es link:www.jmpicon.com[www.jmpicon.com,window="_blank"] y mi Mail es jmpicon@jmpicon.com

== Rubén Gómez
Ingeniero técnico industrial en la rama de electrónica industrial, usuario de Gnu/Linux desde 1998, he colaborado en distintas comunidades del software libre y proyectos desde entonces, actualmente totalmente involucrado con la comunidad y macroproyecto KDE desde https://www.kde-espana.org[KDE España,window="_blank"] y con el colectivo https://hacklabalmeria.net/[Hacklab Almería,window="_blank"]. También formo parte, desde hace unos meses de https://www.documentfoundation.org/[The Document Foundation,window="_blank"].»


== David Vaquero
Formador,programador y podcaster , Republica Web(https://republicaweb.es/[web,window="_blank"]) sobre desarrollo web y Formadores en Tiempos Revueltos (link:anchor.fm/foentire[anchor.fm/foentire,window="_blank"]). Creador de la web Cursos de Desarrollo(https://cursosdedesarrollo.com/[web,window="_blank"]) donde publica interesantes artículos y cursos de programación.



Imagen de  https://unsplash.com/@digitalmike?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText[Michal Czyz ]