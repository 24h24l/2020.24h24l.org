= Redes y Seguridad
include::includes/header.adoc[]
:jbake-type: page
:jbake-tags: 
:jbake-status: published
:withtitle: true

image::images/banner-network.jpg[link=redes.html]

//include::includes/menu.adoc[]

En esta categoría tendremos  una serie de audio relacionadas con redes como la seguridad, con dos moderadores con amplia expericia en el ámbito de la redes  y  los participantes su experiencia en la temática del audio.


1. Hosting y Linux: Eduardo Collado, participantes  Jorge(@cegroj) y  @Al_Demon
2. Monitorización de redes; Samuel participante David (ochobits)
3. Seguridad(VPN, auditorias..); Samuel  @Josuer08

**Moderadores:** link:ponente-01.html#_samuel[Samuel] y link:ponente-01.html#_eduardo_collado[Eduardo Collado]